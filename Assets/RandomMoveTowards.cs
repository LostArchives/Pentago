﻿using UnityEngine;
using System.Collections;

public class RandomMoveTowards : MonoBehaviour {

    public GameObject[] targets;
    public float Speed;

	// Use this for initialization
	void Start () {
        StartCoroutine(MoveTowards());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator MoveTowards()
    {
        System.Random r = new System.Random();
        int actualTarget = 0;
        int newTarget = 0;

        for (int cntPlayer = 0; cntPlayer < 100; cntPlayer++)
        {

            while(actualTarget == newTarget)
            {
                actualTarget = r.Next(0, targets.Length);
                yield return null;
            }

            while(transform.position!=targets[actualTarget].transform.position)
            {
                transform.position = Vector3.MoveTowards(transform.position, targets[actualTarget].transform.position, Speed*Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(.2f);
            newTarget = actualTarget;

        }

        yield return null;

    }
}
