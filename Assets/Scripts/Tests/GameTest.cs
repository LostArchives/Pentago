﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class GameTest : MonoBehaviour {

    private GameManager _gm;

    [SerializeField] GameObject BallPrefab;
    [SerializeField] TurnList turnList;
    
    void Awake()
    {
        _gm = GameManager.Instance;
    }

	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlaceBall(string position)
    {
      
        GameObject ball = Instantiate(BallPrefab.gameObject);
        ball.transform.SetParent(_gm.FullBoard[position].transform, false);

        ball.GetComponent<Image>().color = _gm.ActualColor.Color;
        _gm.FullBoard[position].GetComponent<CellInformation>().Content = _gm.ActualColor.Name;
            
    }

    public void PlaceBalls()
    {
        PlaceBall("a0");
        PlaceBall("a1");
        PlaceBall("b0");
        PlaceBall("b1");
        PlaceBall("c0");
        PlaceBall("c1");
        PlaceBall("c2");
        PlaceBall("a2");
    }

    public void StartRandomPlacer()
    {
        StartCoroutine(RandomGame());
    }

    private IEnumerator RandomGame()
    {
        turnList.gameObject.SetActive(true);
        string[] winning = new string[_gm.SelectedGameMode.RequiredAlignement];
        int colorIndex = 0;
        bool found = false;
        
        System.Random r = new System.Random();

        for (int cntBall = 0; cntBall < _gm.FullBoard.Count;cntBall++)
        {
            yield return new WaitForSeconds(.1f);
            _gm.ActualColor = _gm.SelectedGameMode[colorIndex];
            int index = r.Next(_gm.FreePositions.Count);
            PlaceBall(_gm.FreePositions.ElementAt(index));
            _gm.FreePositions.Remove(_gm.FreePositions.ElementAt(index));
            yield return new WaitForSeconds(.005f);
            
            int sbindex = r.Next(_gm.SubBoards.Count);
            _gm.SelectedSubBoard = _gm.SubBoards[sbindex];
            
            int rotation = r.Next(3);
            if (rotation!=2)
            {
                float speed = (rotation == 0 ? -15f : 15f);
                Rotation rot = _gm.SelectedSubBoard.GetComponent<Rotation>();
                yield return StartCoroutine(rot.rotate(new Vector3(0,0,speed)));
                
            }
            
            string win = CheckWinLine("horiz");
            yield return win;
            if (win != "")
            {
                Debug.Log(win);
                winning = win.Split(';');
                found = true;
                break;
            }
            win = CheckWinLine("vert");
            yield return win;
            if (win != "")
            {
                Debug.Log(win);
                winning = win.Split(';');
                found = true;
                break;
            }
           
            win = CheckWinDiag(true);
            yield return win;
            if (win != "")
            {
                Debug.Log(win);
                winning = win.Split(';');
                found = true;
                break;
            }
            win = CheckWinDiag(false);
            yield return win;
            if (win != "")
            {
                Debug.Log(win);
                winning = win.Split(';');
                found = true;
                break;
            }

            colorIndex++;
            if (colorIndex >= _gm.SelectedGameMode.CountColor())
                colorIndex = 0;

            yield return new WaitForSeconds(.5f);
            if (cntBall < _gm.FullBoard.Count - 1)
            yield return StartCoroutine(turnList.ScrollToNext());
            
        }
        if (found)
        foreach (string s in winning)
        {
            
                Animator anim = _gm.FullBoard[s].transform.GetChild(0).GetComponent<Animator>();
                anim.SetBool("Blink", true);
                yield return new WaitForSeconds(.05f);
               
        }
        
        yield return null;
    }

   
    private string CheckWinLine(string mode)
    {
        string win = "";
        int nbAlign = 1;
        for (int cntLine = 0; cntLine < _gm.BoardLength; cntLine++)
        {
            for (int cntColumn = 0; cntColumn < _gm.BoardLength - 1; cntColumn++)
            {
                string coordinate = (mode == "horiz" ? (char)(cntLine + 97) + "" + cntColumn : (char)(cntColumn + 97) + "" + cntLine);
                string nextcoordinate = (mode == "horiz" ? (char)(cntLine + 97) + "" + (cntColumn+1) : (char)(cntColumn + 1 + 97) + "" + cntLine);
                
                if (_gm.FullBoard[coordinate].transform.childCount!=0)
                if (_gm.FullBoard[coordinate].GetComponent<CellInformation>().Content.Equals(
                    _gm.FullBoard[nextcoordinate].GetComponent<CellInformation>().Content))
                {
                    nbAlign++;
                    win += coordinate+";";
                    if (nbAlign == _gm.SelectedGameMode.RequiredAlignement)
                    {
                            win += nextcoordinate;
                            return win;
                    }
                }
                else
                {
                    nbAlign = 1;
                    win = "";
                }
                
            }
            nbAlign = 1;
            win = "";  
        }

        return win;
    }

    private string[][] GetDiagonal(bool toptoBottom)
    {

        int offset = _gm.SelectedGameMode.RequiredAlignement - 1;
        string[][] diagonals = new string[_gm.BoardLength * 2 - 1 - 2*offset][];
        for (int cntDiagonal = offset; cntDiagonal < _gm.BoardLength * 2 - 1 - offset; cntDiagonal++)
        {
            int diagSize = (cntDiagonal < _gm.BoardLength ? cntDiagonal + 1 : _gm.BoardLength * 2 - (cntDiagonal + 1));
            
            diagonals[cntDiagonal-offset] = new string[diagSize];
            int diagLine = (cntDiagonal < _gm.BoardLength ? 0 : cntDiagonal - _gm.BoardLength + 1);
            int diagColumn = (cntDiagonal < _gm.BoardLength ? cntDiagonal : _gm.BoardLength - 1);

            if (!toptoBottom)
            diagLine = (cntDiagonal < _gm.BoardLength ?
                        _gm.BoardLength - 1 : 2 * (_gm.BoardLength - 1) - cntDiagonal);

            for (int diagCell = 0; diagCell < diagSize; diagCell++)
            {
                    diagonals[cntDiagonal-offset][diagCell] = (char)(diagLine + 97) + "" + diagColumn;
                    diagLine = (toptoBottom ? diagLine + 1 : diagLine - 1);
                    diagColumn--;
                      
            }
            
        }

        return diagonals;
    }

    private string CheckWinDiag(bool toptoBottom)
    {
        string winCoord = "";
        int nbAlign = 1;
        string[][] diag = GetDiagonal(toptoBottom);

        for (int cntDiag = 0; cntDiag < diag.Length; cntDiag++)
        {
            for (int cntCell = 0; cntCell < diag[cntDiag].Length - 1; cntCell++)
            {
                string coordinate = diag[cntDiag][cntCell];
                string nextcoordinate = diag[cntDiag][cntCell + 1];
                if (_gm.FullBoard[coordinate].GetComponent<CellInformation>().Content!="empty")
                if (_gm.FullBoard[coordinate].GetComponent<CellInformation>().Content ==
                    _gm.FullBoard[nextcoordinate].GetComponent<CellInformation>().Content)
                {
                        nbAlign++;
                        winCoord += coordinate+";";
                }
                else
                {
                        nbAlign = 1;
                        winCoord = "";
                }

                if (nbAlign==_gm.SelectedGameMode.RequiredAlignement)
                {
                    winCoord += nextcoordinate;
                    return winCoord;
                }
            }

            nbAlign = 1;
            winCoord = "";

        }

        return winCoord;

    }

    public void RandomSubboardSelection()
    {
        StartCoroutine(RandomSubBoard());
    }

    public void RandomBallSelection()
    {
        StartCoroutine(RandomBall());
    }

    private IEnumerator RandomSubBoard()
    {
        System.Random r = new System.Random();
        int randomIndex = 0;
        int newRandom = 0;
        for (int cntSubBoard = 0; cntSubBoard < 100; cntSubBoard++)
        {
            while(newRandom==randomIndex)
            {
                randomIndex = r.Next(0, _gm.SubBoards.Count);
                yield return null;
            }

            Animator anim = _gm.SubBoards[randomIndex].transform.parent.GetComponent<Animator>();
            anim.SetBool("Selected", true);
            if (cntSubBoard > 75)
                yield return new WaitForSeconds(cntSubBoard / 500f);
            else
                yield return new WaitForSeconds(cntSubBoard / 1000f);

            if (cntSubBoard!=99)
                anim.SetBool("Selected", false);
            
            yield return new WaitForSeconds(.02f);

            newRandom = randomIndex;

        }

        _gm.SelectedSubBoard = _gm.SubBoards[randomIndex];

        yield return null;
    }

    private IEnumerator RandomBall()
    {
        System.Random r = new System.Random();
        string randomCoordinate = RandomCoordinate(r);
        string newCoordinate = "";

        for (int cntCell = 0; cntCell < 100;cntCell++)
        {
            newCoordinate = randomCoordinate;
            while(randomCoordinate==newCoordinate)
            {
                randomCoordinate = RandomCoordinate(r);
                yield return null;
            }
            CellSelector cs = _gm.FullBoard[randomCoordinate].GetComponent<CellSelector>();
            cs.PreviewBall(new Color());

            if (cntCell > 75)
                yield return new WaitForSeconds(cntCell / 500f);
            else
                yield return new WaitForSeconds(cntCell / 1000f);

            cs.PreviewOff();

            yield return new WaitForSeconds(.02f);

            newCoordinate = randomCoordinate;

        }

        PlaceBall(randomCoordinate);

        yield return null;
    }

    private string RandomCoordinate(System.Random generator)
    {
        string coordinate = "";
        string randomLine = (char)(generator.Next(0, _gm.BoardLength)+97)+"";
        int randomColumn = generator.Next(0, _gm.BoardLength);
        coordinate = randomLine + "" + randomColumn;
        return coordinate;
    }

}
