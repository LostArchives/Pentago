﻿public interface IClonable<U> {

    U Clone();

}
