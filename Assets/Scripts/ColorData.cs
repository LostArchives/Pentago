﻿using UnityEngine;
using System.Collections;

public class ColorData : MonoBehaviour {

    public ColorElement ColorElement;
    private bool finished = false;

    public bool isFinished
    {
        get { return finished; }
    }

    public IEnumerator MoveToCorrectPlace()
    {
        finished = false;
       float[] distance = new float[2];
        distance[0] = 1f;
        distance[1] = 1f;

       while(distance[0] > .001f || distance[1] > .001f)
       {

        transform.GetChild(0).position = Vector3.MoveTowards(transform.GetChild(0).position, transform.parent.GetChild(ColorElement.Order).position, 50f * Time.deltaTime);
        transform.GetChild(1).position = Vector3.MoveTowards(transform.GetChild(1).position, transform.parent.GetChild(ColorElement.Order).position, 50f * Time.deltaTime);
        yield return null;
        distance[0] = Vector3.Distance(transform.GetChild(0).position, transform.parent.GetChild(ColorElement.Order).position);
        distance[1] = Vector3.Distance(transform.GetChild(1).position, transform.parent.GetChild(ColorElement.Order).position);

        }


        transform.GetChild(0).SetParent(transform.parent.GetChild(ColorElement.Order));
        transform.GetChild(0).SetParent(transform.parent.GetChild(ColorElement.Order));
        finished = true;
        yield return null;
    }

}
