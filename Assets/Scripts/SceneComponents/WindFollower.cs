﻿using UnityEngine;

public class WindFollower : MonoBehaviour {

    public string Mode;
    public float[] Limits;
    public float MaxSpeed;
    public float RandomSpeed;
    private float MinSpeed = 35f;
    private System.Random r;

    // Use this for initialization
    void Start () {
        Mode = "left";
        r = new System.Random();
    }
	
	// Update is called once per frame
	void Update () {
        RandomSpeed = MinSpeed + (MaxSpeed - MinSpeed) * (float) r.NextDouble();
        Rotate(RandomSpeed);

    }

    private void Rotate(float speed)
    {

       if (Mode == "right")
       {
            transform.RotateAround(transform.Find("Center").transform.position, new Vector3(0, 0, 1), Time.deltaTime * -speed);
       }
       else if (Mode == "left")
       {
            transform.RotateAround(transform.Find("Center").transform.position, new Vector3(0, 0, 1), Time.deltaTime * speed);
       }

       if (transform.eulerAngles.z > Limits[0] && transform.eulerAngles.z < Limits[1])
       {
            Mode = "right";     
       }
       else if (transform.eulerAngles.z >Limits[2] && transform.eulerAngles.z < Limits[3])
       {
            Mode = "left";
       }

    }
}
