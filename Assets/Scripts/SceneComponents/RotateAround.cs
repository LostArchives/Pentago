﻿using UnityEngine;
using System.Collections;

public class RotateAround : MonoBehaviour {

    public GameObject[] ObjectsToRotate;
    public GameObject Center;
    public float Speed;

	// Use this for initialization
	void Start () {
        StartCoroutine(RandomRotate());
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private IEnumerator RandomRotate()
    {
        while(true)
        {
            yield return new WaitForSeconds(1f);

            System.Random r = new System.Random();
            int rotateFactor = (r.Next(3) + 1) * 45;
            for (int cntRotate = 0; cntRotate < rotateFactor; cntRotate++)
            {

                transform.eulerAngles -= new Vector3(0, 0, Speed);
                yield return new WaitForEndOfFrame();
            }
            
            yield return new WaitForSeconds(.2f);

            int letterToRotate = r.Next(ObjectsToRotate.Length);
            int letterFactor = (r.Next(3) + 1) * 45;
            for (int cntletterRotate = 0; cntletterRotate < letterFactor; cntletterRotate++)
            {
                ObjectsToRotate[letterToRotate].transform.eulerAngles -= new Vector3(0, 0, Speed);
                yield return new WaitForSeconds(.01f);
            }

            yield return new WaitForSeconds(.1f);
        }
       
    }
}
