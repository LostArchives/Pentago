﻿using UnityEngine;

public class MoveTowards : MonoBehaviour {

    public Transform Target;
    public float Speed;

	// Use this for initialization
	void Start () {
        transform.position = new Vector3(transform.position.x, Target.position.y + 10f, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, Target.position, Speed * Time.deltaTime);
	}
}
