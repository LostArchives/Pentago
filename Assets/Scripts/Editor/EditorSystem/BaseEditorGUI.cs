﻿using UnityEngine;
using UnityEditor;

public abstract partial class BaseEditor<T,U> {

    const float SPACE_SIZE = 10f;

    protected GUIStyle _titleStyle;
    protected GUIStyle _textStyle;
    protected GUIStyle _buttonStyle;
    protected GUIStyle _textfieldStyle;
    protected GUIStyle _textareaStyle;

    protected void Title(params string[] text)
    {
        GUILayout.BeginVertical(GUI.skin.box);

        for (int cntLine = 0; cntLine < text.Length; cntLine++)
        {
            WriteLine(text[cntLine],_titleStyle);
        }

        GUILayout.EndVertical();

        GUILayout.Space(SPACE_SIZE);
        GUILayout.Space(SPACE_SIZE);
    }

    protected  void Header()
    {
        WriteLine("Number of " + typeof(U).ToString() + "(s) : " + myDb.Count, _textStyle);
        GUILayout.Space(SPACE_SIZE);
    }

    protected void RemoveArea(int index)
    {
        GUILayout.BeginHorizontal();

        GUILayout.FlexibleSpace();

        if (GUILayout.Button("X", _buttonStyle, GUILayout.ExpandWidth(true)))
        {
            myDb.RemoveAt(index);
            isRemoved = true;
        }

        GUILayout.EndHorizontal();
        GUILayout.Space(SPACE_SIZE);
    }

    protected void HUD()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Add " + typeof(U).ToString(), _buttonStyle, GUILayout.Height(75f), GUILayout.ExpandHeight(false)))
        {
            myDb.Add(System.Activator.CreateInstance<U>());
        }
        if (GUILayout.Button("Save Database", _buttonStyle, GUILayout.Height(75f), GUILayout.ExpandHeight(false)))
        {
            EditorUtility.SetDirty(myDb);
            Debug.Log("Saved Successfully :)");
        }
        GUILayout.EndHorizontal();

    }

    protected void PrepareStyles()
    {
        _titleStyle = GUIStyleManager.TextStyle("cuttsink", FontStyle.Bold, 40, Color.black);
        _textStyle = GUIStyleManager.TextStyle("cuttsink", FontStyle.Bold, 20, Color.white);
        _buttonStyle = GUIStyleManager.ButtonStyle("cuttsink", FontStyle.Bold, 20, Color.black);
        _textfieldStyle = GUIStyleManager.TextFieldStyle("cuttsink", FontStyle.Bold, 20, Color.black);
        _textareaStyle = GUIStyleManager.TextAreaStyle("cuttsink", FontStyle.Bold, 20, Color.black);
    }


    #region Private Methods



    private void WriteLine(string line, GUIStyle style)
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(line, style);
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    #endregion

}
