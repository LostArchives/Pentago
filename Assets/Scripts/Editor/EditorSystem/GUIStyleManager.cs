﻿using UnityEngine;

public static class GUIStyleManager  {

        public static GUIStyle TextStyle(FontStyle fontstyle, int fontsize, Color fontcolor)
        {
            GUIStyle g = new GUIStyle();
            g.font = Resources.Load<Font>("Fonts/cuttsink");
            g.fontStyle = fontstyle;
            g.fontSize = fontsize;
            g.normal.textColor = fontcolor;
            return g;
        }

        public static GUIStyle TextStyle(string fontname, FontStyle fontstyle, int fontsize, Color fontcolor)
        {
            GUIStyle g = new GUIStyle();
            g.font = Resources.Load<Font>("Fonts/" + fontname);
            g.fontStyle = fontstyle;
            g.fontSize = fontsize;
            g.normal.textColor = fontcolor;
            g.onHover.textColor = Color.white;
            return g;
        }

        public static GUIStyle ButtonStyle(string fontname, FontStyle fontstyle, int fontsize, Color fontcolor)
        {
            GUIStyle g = new GUIStyle(GUI.skin.button);
            g.font = Resources.Load<Font>("Fonts/" + fontname);
            g.fontStyle = fontstyle;
            g.fontSize = fontsize;
            g.normal.textColor = fontcolor;
            return g;
        }

        public static GUIStyle TextFieldStyle(string fontname, FontStyle fontstyle, int fontsize, Color fontcolor)
        {
            GUIStyle g = new GUIStyle(GUI.skin.textField);
            g.font = Resources.Load<Font>("Fonts/" + fontname);
            g.fontStyle = fontstyle;
            g.fontSize = fontsize;
            g.normal.textColor = fontcolor;
            return g;
        }

        public static GUIStyle TextAreaStyle(string fontname, FontStyle fontstyle, int fontsize, Color fontcolor)
        {
        GUIStyle g = new GUIStyle(GUI.skin.textArea);
        g.font = Resources.Load<Font>("Fonts/" + fontname);
        g.fontStyle = fontstyle;
        g.fontSize = fontsize;
        g.normal.textColor = fontcolor;
        return g;
        }

    public static Texture2D TextureMaker(int width, int height, Color color)
        {
            Color[] pixels = new Color[width * height];
            for (int cntPix = 0; cntPix < pixels.Length; cntPix++)
            {
                pixels[cntPix] = color;
            }
            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pixels);
            result.Apply();
            return result;
        }

    }

