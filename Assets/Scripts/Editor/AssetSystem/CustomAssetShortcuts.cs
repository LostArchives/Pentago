﻿using UnityEditor;

public class CustomAssetShortcuts {

	[MenuItem("Paletto/Create/Game Mode Database")]
    public static void CreateGameMode()
    {
        CustomAssetUtility.CreateAsset<GameModeDatabase>();
    }

    [MenuItem("Paletto/Create/Character Database")]
    public static void CreateCharacter()
    {
        CustomAssetUtility.CreateAsset<CharacterDatabase>();
    }
}
