﻿using UnityEngine;

public class DataBaseLinker : MonoBehaviour {

    public GameModeDatabase GameModeDatabase;
    public CharacterDatabase CharacterDataBase;
    private GameManager _gm;

    void Awake()
    {
        _gm = GameManager.Instance;
    }

    public void SetSelectedGameMode(int index)
    {
        _gm.SelectedGameMode = GameModeDatabase[index];
    }
}
