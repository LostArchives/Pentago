﻿using UnityEngine;

public class AutoScrollText : MonoBehaviour {

    private RectTransform _parentRct;
    private RectTransform _rct;
    private float _startMin;

    void Awake()
    {
        _rct = GetComponent<RectTransform>();
        _parentRct = transform.parent.GetComponent<RectTransform>();  
    }

	// Use this for initialization
	void Start ()
    {
        _startMin = (_rct.rect.width - _parentRct.rect.width) / 2f;
        _rct.localPosition = new Vector3(_startMin, _rct.localPosition.y, _rct.localPosition.z);
    }
	
	// Update is called once per frame
	void Update ()
    {
        _startMin = (_rct.rect.width - _parentRct.rect.width) / 2f;

        if (_rct.rect.width > _parentRct.rect.width)
        {
           _rct.offsetMin = new Vector2(_rct.offsetMin.x - 2, _rct.offsetMin.y);
           _rct.offsetMax = new Vector2(_rct.offsetMax.x - 2, _rct.offsetMax.y);

            if (-_rct.offsetMax.x > 100f)
            {
                _rct.localPosition = new Vector3(_startMin, _rct.localPosition.y, _rct.localPosition.z);
            }  
        }
        else
        {
            _rct.localPosition = new Vector3(_startMin, _rct.localPosition.y, _rct.localPosition.z);
        }
        
    }

}
