﻿using UnityEngine;
using UnityEngine.UI;

public class SelectionPanelManager : MonoBehaviour {

    public SelectionPanel ActualPanel;
    public SelectionPanel[] AllPanels;
    public Text PageCount;
    public Text InstructionField;
    public int indexPanel;

    // Use this for initialization
    void Start ()
    {
        indexPanel = 0;
        AllPanels[indexPanel] = ActualPanel;
        OpenPanel(ActualPanel);
        
    }
	

    private void OpenPanel(SelectionPanel panel)
    {
        if (ActualPanel != null)
            ActualPanel.IsOpen = false;

        ActualPanel = panel;
        ActualPanel.IsOpen = true;
        PageCount.text = (indexPanel + 1).ToString() + "/" + AllPanels.Length;
        InstructionField.text = AllPanels[indexPanel].instruction;
    }

    public void PreviousPanel()
    {
        indexPanel--;
        if (indexPanel < 0)
            indexPanel = AllPanels.Length - 1;

        OpenPanel(AllPanels[indexPanel]);    
    }

    public void NextPanel()
    {
        indexPanel++;
        if (indexPanel > AllPanels.Length - 1)
            indexPanel = 0;

        OpenPanel(AllPanels[indexPanel]);
    }

}
