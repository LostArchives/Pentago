﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NumberOfPlayers : MonoBehaviour {

    private GameManager _gm;
    private int _selectedNbPlayer;
    public PlayerList PlayerList;
    public Text NbPlayerField;

    void Awake()
    {
        _gm = GameManager.Instance;
    }
    
	// Use this for initialization
	void Start () {
        _selectedNbPlayer = 2;
        NbPlayerField.text = _selectedNbPlayer + "" ;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ChangeNbPlayer(int toAdd)
    {
        if (_gm.SelectedGameMode.Name !="")
        {
            _selectedNbPlayer += toAdd;
            if (_selectedNbPlayer > _gm.SelectedGameMode.MaxPlayer)
                _selectedNbPlayer = _gm.SelectedGameMode.MaxPlayer;

            if (_selectedNbPlayer < _gm.SelectedGameMode.MinPlayer)
                _selectedNbPlayer = _gm.SelectedGameMode.MinPlayer;

            NbPlayerField.text = _selectedNbPlayer + "";
            PlayerList.BuildPlayerList(_selectedNbPlayer);
        }
        

    }

    public void SetNbPlayer(int val)
    {
        _selectedNbPlayer = val;
        PlayerList.BuildPlayerList(_selectedNbPlayer);
        NbPlayerField.text = _selectedNbPlayer + "";
    }

   
}
