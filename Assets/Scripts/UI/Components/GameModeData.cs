﻿using UnityEngine;
using UnityEngine.UI;


public class GameModeData : MonoBehaviour {

    public GameMode MyGameMode;
    public GameModeList MyParent;
    public GameObject ColorPrefab;
    private Animator anim;
    private GameManager _gm;

    void Awake()
    {
        anim = GetComponent<Animator>();
        _gm = GameManager.Instance;
    }

    public void MouseOver(bool over)
    {
        anim.SetBool("MouseOver", over);
        if (!over)
            MyParent.FooterPanel.GetComponent<Text>().text = "no element selected";
    }

    public void Selected()
    {
        if (MyParent.SelectedGameMode!=null)
            MyParent.SelectedGameMode.GetComponent<Animator>().SetBool("Selected", false);
        
        MyParent.SelectedGameMode = gameObject;
        _gm.SelectedGameMode = MyGameMode;

        MyParent.NumberOfPlayers.SetNbPlayer(_gm.SelectedGameMode.MinPlayer);

        MyParent.SelectedModeLabel.text = "game mode selected\n" + MyGameMode.Name;
        SelectedAnim(true);
        
    }

    private void SelectedAnim(bool select)
    {
        anim.SetBool("Selected", select);
    }

    public void ShowInfo()
    {
        MyParent.TurnOrderLabel.enabled = true;
        MyParent.DescriptionLabel.transform.parent.GetComponent<Image>().enabled = true;
        MyParent.DescriptionLabel.enabled = true;
        MyParent.PlayerNumberLabel.enabled = true;

        MyParent.DescriptionPanel.GetComponent<Text>().text = "details : " + MyGameMode.Name;
        MyParent.PlayerNumberLabel.text = "min player - " + MyGameMode.MinPlayer + "\n" 
                                            + "max player - " + MyGameMode.MaxPlayer;

        MyParent.DescriptionLabel.text = MyGameMode.Description;
        MyParent.FooterPanel.GetComponent<Text>().text = MyGameMode.Comment;   
    }

    public void BuildColorList()
    {
        DestroyAllChild();
        
        for (int cntColor = 0; cntColor < MyGameMode.CountColor();cntColor++)
        {
            GameObject colorElement = (GameObject) Instantiate(ColorPrefab,MyParent.ColorPanel.transform, false);
            colorElement.transform.GetChild(0).GetComponent<Text>().text = cntColor + 1 + ".";
            colorElement.transform.GetChild(1).GetComponent<Image>().color = MyGameMode[cntColor].Color;

        }
        
    }

    private void DestroyAllChild()
    {
        for (int cntChild = 0; cntChild < MyParent.ColorPanel.transform.childCount; cntChild++)
        {
            Destroy(MyParent.ColorPanel.transform.GetChild(cntChild).gameObject);
        }
    }
 
}
