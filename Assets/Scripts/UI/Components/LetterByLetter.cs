﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LetterByLetter : MonoBehaviour {

    private Text _textComponent;
    private string _content;
    public float Speed;
    public float FadeDuration;
    
    void Awake() {
        _textComponent = GetComponent<Text>();
        _textComponent.CrossFadeAlpha(0, 0f, true);
    }

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}


    private IEnumerator DisplayLByL()
    {
        _content = _textComponent.text.ToString();
        _textComponent.text = "";
        
        _textComponent.enabled = true;
        _textComponent.CrossFadeAlpha(1,FadeDuration, true);
        
        while (!_textComponent.enabled)
            yield return null;

        for (int cnt = 0; cnt < _content.Length; cnt++)
        {
            _textComponent.text += _content[cnt];
            yield return new WaitForSeconds(1/Speed);

        }
        

    }

    public IEnumerator DisplayLByL(string textToDisplay)
    {
        _textComponent.text = "";
        _textComponent.CrossFadeAlpha(0f, 0f, true);
        _textComponent.enabled = true;
        _textComponent.CrossFadeAlpha(1,FadeDuration, true);
        for (int cnt = 0; cnt < textToDisplay.Length; cnt++)
        {
            _textComponent.text += textToDisplay[cnt];
            yield return new WaitForSeconds(1 / Speed);
        }
        
    }


    public void Display()
    {
        StartCoroutine(DisplayLByL());
    }

    public void Display(string textToDisplay)
    {
        StartCoroutine(DisplayLByL(textToDisplay));
    }


}
