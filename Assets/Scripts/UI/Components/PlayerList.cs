﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerList : MonoBehaviour {

    public GameObject CharacterPrefab;
    public GameObject BioPanel;
    public GameObject ContentPanel;
    private GameManager _gm;
    
    void Awake()
    {
        _gm = GameManager.Instance;
    }

    void Start() {    }

    public void BuildPlayerList(int nbPlayer)
    {
        if (_gm.Players == null)
            _gm.Players = new List<Player>();
        else
            _gm.Players.Clear();

        if (transform.childCount > 0)
            DestroyAllChild();

        for (int cntPlayer = 0; cntPlayer <  nbPlayer; cntPlayer++)
        {

            Player _player = new Player();

            GameObject character = (GameObject)Instantiate(CharacterPrefab,transform,false);

            character.GetComponent<PlayerData>().Player = _player;
            character.GetComponent<PlayerData>().PlayerId.text = "player " + (cntPlayer + 1);
            _gm.Players.Add(_player);

        }

    }

    public void DestroyAllChild()
    {
        for (int cntChild = 0; cntChild < transform.childCount;cntChild++)
        {
            Destroy(transform.GetChild(cntChild).gameObject);
        }
    }

}
