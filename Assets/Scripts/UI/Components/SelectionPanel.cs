﻿using UnityEngine;

public class SelectionPanel : MonoBehaviour {

    private CanvasGroup _canvasGroup;
    private Animator _animator;
    public string instruction;

    void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _animator = GetComponent<Animator>();   
    }

    public bool IsOpen
    {
        get { return _animator.GetBool("IsOpen"); }
        set
        {
            _animator.SetBool("IsOpen", value);
            _canvasGroup.blocksRaycasts = _canvasGroup.interactable = value;
        }
    }


    
}
