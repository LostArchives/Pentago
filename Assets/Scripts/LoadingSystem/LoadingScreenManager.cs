﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingScreenManager : MonoBehaviour {

    public float FadeDuration;
    public float WaitOnFinishedload;
    public Image Fader;
    public Image ProgressBar;
    public Text ProgressText;

    private string actualScene;
    public static string sceneToLoad = "NoSceneToLoad";
    public static string LoadingScene = "LoadingScreen";
    private AsyncOperation asyncOp;


	// Use this for initialization
	void Start () {
        actualScene = SceneManager.GetActiveScene().name;
        if (sceneToLoad != "NoSceneToLoad")
        {
            
            ProgressBar.fillAmount = 0f;
            ProgressText.text = "0 o/o";
            Fader.color = new Color(Fader.color.r, Fader.color.g, Fader.color.b, 1f);
            StartCoroutine(TransitionScene());
        }
            
	}
	
	// Update is called once per frame
	void Update () {
       
    }

    public static void LaunchScene(string sceneName)
    {
        sceneToLoad = sceneName;
        SceneManager.LoadScene(LoadingScene);

    }

    private IEnumerator TransitionScene()
    {

        FadeIn();

        asyncOp = SceneManager.LoadSceneAsync(sceneToLoad,LoadSceneMode.Single);
        asyncOp.allowSceneActivation = false;

        float lastprogress = 0f;
        
        while (!FinishedLoading() || ProgressBar.fillAmount < .99f)
        {
            
            if (Mathf.Approximately(asyncOp.progress, lastprogress) == false)
            {
                
               
            }
            lastprogress = asyncOp.progress;
            ProgressBar.fillAmount = asyncOp.progress + .1f;
            ProgressText.text = (int)(lastprogress*100f) + " o/o";
            yield return new WaitForEndOfFrame();


        }

        ProgressBar.fillAmount = 1f;
        ProgressText.text = "100 o/o";
        yield return new WaitForSeconds(WaitOnFinishedload);

        FadeOut();
        yield return new WaitForSeconds(FadeDuration);

        asyncOp.allowSceneActivation = true;
        
        sceneToLoad = "NoSceneToLoad";
    }


    private bool FinishedLoading()
    {
        return (asyncOp.progress >=.9f);
    }

    private void FadeIn()
    {
        Fader.CrossFadeAlpha(0, FadeDuration, true);
    }

    private void FadeOut()
    {
        Fader.CrossFadeAlpha(1f, FadeDuration, true);
    }
}
