﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TurnList : MonoBehaviour {

    private ScrollRect _scrollRect;
    private RectTransform _rct;
    private VerticalLayoutGroup _vlg;
    private GameObject _turnList;
    private RectTransform _turnListRct;
    private GameManager _gm;
    private int _nbTurn;

    public GameObject turnElement;
    public CharacterDatabase cdb;
    public int turnListSize;
    public int maxTurnVisible;
    public int turnNotVisible
    {
        get { return turnListSize - maxTurnVisible; }
    }

    void Awake()
    {
        _scrollRect = GetComponent<ScrollRect>();
        _rct = GetComponent<RectTransform>();
        _vlg = transform.GetChild(0).GetComponent<VerticalLayoutGroup>();
        _turnList = transform.Find("TurnList").gameObject;
        _turnListRct = _turnList.GetComponent<RectTransform>();
        _gm = GameManager.Instance;
    }

	// Use this for initialization
	void Start () {

        _nbTurn = 1;
        BuildTurnList();
        _turnList.transform.GetChild(0).GetComponent<Animator>().SetBool("Actual", true);

    }
	
	// Update is called once per frame
	void Update () {
       

    }

    
    public void DebugBuildTurnList()
    {
        float elementHeight = 0f;

        for (int cntTurn = 0; cntTurn < turnListSize; cntTurn++)
        {
            GameObject turnObject = (GameObject) Instantiate(turnElement, _turnList.transform , false);
            turnObject.GetComponent<LayoutElement>().preferredHeight = (_rct.rect.height - ((maxTurnVisible - 1) * _vlg.spacing)) / maxTurnVisible;
            turnObject.name = "turn_"+cntTurn;
            elementHeight = turnObject.GetComponent<LayoutElement>().minHeight = (_rct.rect.height - ((maxTurnVisible - 1) * _vlg.spacing)) / maxTurnVisible;
            turnObject.transform.GetChild(0).GetComponent<Image>().sprite = cdb[cntTurn%maxTurnVisible].Portrait;
            turnObject.transform.GetChild(1).GetComponent<Image>().color = Color.blue;

        }

        _turnListRct.sizeDelta = new Vector2(_turnListRct.sizeDelta.x, turnNotVisible * (elementHeight + _vlg.spacing));
        _scrollRect.verticalNormalizedPosition = 1f;
    }

    public void BuildTurnList()
    {
        float elementHeight = 0f;
        int nbPlayers = _gm.Players.Count;
        for (int cntTurn = 0; cntTurn < turnListSize; cntTurn++)
        {
            GameObject turnObject = (GameObject)Instantiate(turnElement, _turnList.transform, false);
            LayoutElement lay = turnObject.GetComponent<LayoutElement>();
            lay.preferredHeight = (_rct.rect.height - ((maxTurnVisible - 1) * _vlg.spacing)) / maxTurnVisible;

            elementHeight = lay.minHeight = (_rct.rect.height - ((maxTurnVisible - 1) * _vlg.spacing)) / maxTurnVisible;

            turnObject.transform.GetChild(0).GetComponent<Text>().enabled = _gm.Players[cntTurn % nbPlayers].IsBot;
            turnObject.transform.GetChild(1).GetComponent<Image>().sprite = _gm.Players[cntTurn % nbPlayers].Portrait;
            turnObject.transform.GetChild(2).GetComponent<Image>().color = _gm.Players[cntTurn % nbPlayers].Color;
            
            turnObject.name = "turn_" + cntTurn + "_" + _gm.Players[cntTurn % nbPlayers].Character.Name;

        }

        _turnListRct.sizeDelta = new Vector2(_turnListRct.sizeDelta.x, turnNotVisible * (elementHeight + _vlg.spacing));
        _scrollRect.verticalNormalizedPosition = 1f;

    }

    public void BuildTurnList2()
    {
        float elementHeight = 0f;
        int nbPlayers = _gm.SelectedGameMode.MaxPlayer;
        for (int cntTurn = 0; cntTurn < turnListSize; cntTurn++)
        {
            GameObject turnObject = (GameObject)Instantiate(turnElement, _turnList.transform, false);
            LayoutElement lay = turnObject.GetComponent<LayoutElement>();

            lay.preferredHeight = (_rct.rect.height - ((maxTurnVisible - 1) * _vlg.spacing)) / maxTurnVisible;

            elementHeight = lay.minHeight = (_rct.rect.height - ((maxTurnVisible - 1) * _vlg.spacing)) / maxTurnVisible;
            
            turnObject.transform.GetChild(0).GetComponent<Image>().sprite = cdb[cntTurn % nbPlayers].Portrait;
            turnObject.transform.GetChild(1).GetComponent<Image>().color = _gm.SelectedGameMode[cntTurn % nbPlayers].Color;
            turnObject.name = "turn_" + cntTurn + "_" + cdb[cntTurn % nbPlayers].Name;

        }

        _turnListRct.sizeDelta = new Vector2(_turnListRct.sizeDelta.x, turnNotVisible * (elementHeight + _vlg.spacing));
        _scrollRect.verticalNormalizedPosition = 1f;

    }

    public void StartScrollToNext()
    {
        StartCoroutine(ScrollToNext());
    }

    
    public IEnumerator ScrollToNext()
    {
        _nbTurn++;

        float scrollPosition = _scrollRect.verticalNormalizedPosition;
        float toRemove = (1f/turnNotVisible) / 10f;

        if (scrollPosition>0)
        {
            for (int cntScroll = 0; cntScroll < 10; cntScroll++)
            {
                _scrollRect.verticalNormalizedPosition -= toRemove;
                yield return new WaitForSeconds(.001f);
            }
        }
        
        if (_nbTurn > maxTurnVisible)
        {
            for (int cntNotVisible = 0; cntNotVisible < maxTurnVisible; cntNotVisible++)
            {
                _turnList.transform.GetChild(0).GetComponent<Animator>().SetBool("Actual", false);
                _turnList.transform.GetChild(0).SetAsLastSibling(); 
            }
            _scrollRect.verticalNormalizedPosition = 1f;
            _nbTurn = 1;
        }
        _turnList.transform.GetChild(_nbTurn-1).GetComponent<Animator>().SetBool("Actual", true);
        yield return new WaitForEndOfFrame();

    }



}
