﻿using UnityEngine;

public class Player {

    private Character _character;
    private ColorElement _colorElement;
    private bool _isBot;

    public Character Character
    {
        get { return _character; }
        set { _character = value; }
    }

    public ColorElement ColorElement
    {
        get { return _colorElement; }
        set { _colorElement = value; }
    }

    public bool IsBot
    {
        get { return _isBot; }
        set { _isBot = value; }
    }

    public Color Color
    {
        get { return _colorElement.Color; }
    }

    public Sprite Portrait
    {
        get { return _character.Portrait; }
    }

    public Player()
    {
        _character = new Character();
        _colorElement = new ColorElement();
        _isBot = false;
    }

    public Player(bool isBot)
    {
        _character = new Character();
        _colorElement = new ColorElement();
        _isBot = isBot;
    }
	
}
