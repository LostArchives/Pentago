﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameBoard : MonoBehaviour {

    public GameObject SubBoardPanelPrefab;
    public GameObject CellPrefab;
    public GameObject[][] diagonals;
    private RectTransform _rt;
    private GridLayoutGroup _glg;
    private GameManager _gm;
    
    void Awake()
    {
        _glg = GetComponent<GridLayoutGroup>();
        _rt = GetComponent<RectTransform>();
        _gm = GameManager.Instance;
        _gm.FullBoard = new Dictionary<string, GameObject>();
        _gm.FreePositions = new HashSet<string>();
    }

	// Use this for initialization
	void Start () {

    }

    public void StartTheGame()
    {
        int _space_subBoard = (int)_glg.spacing.x;

        int _total_spacing = _space_subBoard * _gm.SubBoardPerLine;

        int _subBoardLength = (int)(_rt.rect.width - _total_spacing) / _gm.SubBoardPerLine;

        _glg.cellSize = new Vector2(_subBoardLength - _space_subBoard, _subBoardLength - _space_subBoard);

        for (int cntSubBoard = 0; cntSubBoard < _gm.TotalSubBoard; cntSubBoard++)
        {
            GameObject actualSbPanel = Instantiate(SubBoardPanelPrefab);
            _gm.SubBoards.Add(actualSbPanel.transform.Find("SubBoard").gameObject);
            actualSbPanel.transform.SetParent(transform, false);
            actualSbPanel.name = "SubBoardPanel_" + cntSubBoard;
            FillSubBoard(cntSubBoard, actualSbPanel);

        }
    }

    private void FillSubBoard(int sbIndex, GameObject subBoardPanel)
    {
        int siblingIndex = 0;
        for (int cntLine = 0; cntLine < _gm.SubBoardLength; cntLine ++ )
        {
            for (int cntColumn = 0; cntColumn < _gm.SubBoardLength; cntColumn ++ )
            {
                GameObject actualCell = Instantiate(CellPrefab);
                actualCell.transform.SetParent(subBoardPanel.transform.Find("SubBoard"), false);
                actualCell.transform.SetSiblingIndex(siblingIndex);
                int trueLine = sbIndex / _gm.SubBoardPerLine * _gm.SubBoardLength + cntLine;
                int trueColumn = sbIndex % _gm.SubBoardPerLine * _gm.SubBoardLength + cntColumn;
                string coordinate = (char)(trueLine + 97) +""+trueColumn;
                _gm.FullBoard.Add(coordinate, actualCell);
                _gm.FreePositions.Add(coordinate);
                subBoardPanel.transform.Find("SubBoard").GetComponent<SubBoard>().MyCells[cntLine, cntColumn] = coordinate;
                CellInformation ci = actualCell.GetComponent<CellInformation>();
                //ci.SubBoardNum = sbIndex;
                //ci.MainBoardLine = trueLine;
                //ci.MainBoardColumn = trueColumn;
                //ci.SubBoardLine = cntLine;
                //ci.SubBoardColumn = cntColumn;
                ci.Coordinate = coordinate;
                ci.Content = "empty";
                actualCell.name = "Cell_" +coordinate.ToUpper();
                siblingIndex++;
            }
        }
        
    }

    public GameObject[][] GetDiagonal(bool toptoBottom)
    {

        diagonals = new GameObject[_gm.BoardLength*2-1][];
        for (int cntDiagonal = 4; cntDiagonal<_gm.BoardLength*2-1;cntDiagonal++)  {

            int diagSize = (cntDiagonal < _gm.BoardLength ? cntDiagonal+1 : _gm.BoardLength * 2  - (cntDiagonal+1));
            if (diagSize>=5)  {

                diagonals[cntDiagonal] = new GameObject[diagSize];
                int diagLine = (cntDiagonal < _gm.BoardLength ? 0 : cntDiagonal - _gm.BoardLength + 1);
                int diagColumn = (cntDiagonal < _gm.BoardLength ? cntDiagonal : _gm.BoardLength - 1);

                if (!toptoBottom)
                    diagLine = (cntDiagonal < _gm.BoardLength ? 
                        _gm.BoardLength - 1 : 2 * (_gm.BoardLength - 1) - cntDiagonal);

                for (int diagCell = 0; diagCell < diagSize; diagCell++)  {
                    diagonals[cntDiagonal][diagCell] = _gm.FullBoard[(char)(diagLine + 97) + "" + diagColumn];
                    diagLine = (toptoBottom ? diagLine + 1 : diagLine - 1);
                    diagColumn--;
                }

            }
              
        }
        return diagonals;
    }

    

    public string CheckWinLine()
    {
        string win = "";
        int nbAlign;
        for (int cntLine = 0; cntLine < _gm.BoardLength; cntLine++)
        {
            nbAlign = 1;
            for (int cntColumn = 0; cntColumn < _gm.BoardLength; cntColumn++)
            {
                string coordinate = (char)(cntLine + 97) + "" + cntColumn;
                string nextcoordinate = (char)(cntLine + 97) + "" + (cntColumn + 1);
                if (_gm.FullBoard[coordinate].GetComponent<CellInformation>().Content ==
                    _gm.FullBoard[nextcoordinate].GetComponent<CellInformation>().Content)
                {
                    nbAlign++;
                    win += coordinate;
                }
                else
                {
                    nbAlign = 1;
                }
                if (nbAlign == 5)
                {
                    return win;
                }
            }
        }

        return win;
    }


}
