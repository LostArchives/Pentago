﻿using UnityEngine;
using System.Collections;

public class NoRotation : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.eulerAngles = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
        transform.eulerAngles = new Vector3(0, 0, 0);
	}
}
