﻿using UnityEngine;
using UnityEngine.UI;

public class SubBoard : MonoBehaviour {

    private GridLayoutGroup _glgParent;
    private GridLayoutGroup _glg;
    private GameManager _gm;

    public GameObject[] Corners;
    public string[,] MyCells;
    

    void Awake()
    {
        _glg = GetComponent<GridLayoutGroup>();
        _gm = GameManager.Instance;
        MyCells = new string[_gm.SubBoardLength, _gm.SubBoardLength];
    }

    // Use this for initialization
    void Start () {

        _glgParent = transform.parent.parent.GetComponent<GridLayoutGroup>();
        int total_spacing = (int)_glgParent.spacing.x * (_gm.SubBoardLength);
        int cellSize = (int) (_glgParent.cellSize.x - total_spacing) / _gm.SubBoardLength;
        _glg.cellSize = new Vector2(cellSize, cellSize);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PreviewSelection(bool show)
    {
        if (_gm.SelectedSubBoard==null)
        {
            for (int cntCorner = 0; cntCorner < Corners.Length; cntCorner++)
            {
                Corners[cntCorner].GetComponent<Image>().enabled = show;
            }

            Animator anim = transform.parent.GetComponent<Animator>();
            anim.SetBool("Blink", show);
        }
        
    }

    public void SetSelectedSubBoard()
    {
        if (_gm.SelectedSubBoard==null)
        {
            Debug.Log("Je clique");
            _gm.SelectedSubBoard = gameObject;
            Animator anim = transform.parent.GetComponent<Animator>();
            anim.SetBool("Selected", true);
        }
        
    }


}
