﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

    const int DEGREE_ROTATE = 90;
    private GameManager _gm;
    private SubBoard _sb;

    void Awake()
    {
        _gm = GameManager.Instance;
        _sb = GetComponent<SubBoard>();
    }


    public void Rotate(float speed)
    {
        StartCoroutine(rotate(new Vector3(0, 0, speed)));   
    }

    public IEnumerator rotate(Vector3 toAdd)
    {
        int Maxloop = (int)(DEGREE_ROTATE / Mathf.Abs(toAdd.z));
        for (int i = 0; i < Maxloop; i++)
        {
            transform.eulerAngles += toAdd;
            yield return new WaitForEndOfFrame();
        }
        

        if (toAdd.z < 0)
        Transpose("normal");
        else
        Transpose("invert");

        transform.eulerAngles = Vector3.zero;

        yield return new WaitForEndOfFrame();

    }

    public void Transpose(string mode)
    {
        string newCoord = "";
        ExchangedData[,] data = new ExchangedData[_gm.SubBoardLength, _gm.SubBoardLength];
        for (int cntLine = 0; cntLine < _gm.SubBoardLength;cntLine++)
        {
            for (int cntColumn = 0; cntColumn < _gm.SubBoardLength; cntColumn++)
            {
                int transLine = (mode =="normal" ? _gm.SubBoardLength - cntColumn - 1 : cntColumn);
                int transColumn = (mode == "normal" ? cntLine : _gm.SubBoardLength - cntLine - 1);
                newCoord = _sb.MyCells[transLine, transColumn];
                data[cntLine, cntColumn] = new ExchangedData();
               
                if (_gm.FullBoard[newCoord].transform.childCount>0)
                {
                    data[cntLine, cntColumn].BallImage = _gm.FullBoard[newCoord].transform.GetChild(0).gameObject;
                    data[cntLine, cntColumn].Content = _gm.FullBoard[newCoord].GetComponent<CellInformation>().Content;
                }
                else
                {
                    data[cntLine, cntColumn].Content = "empty";
                }
                
            }
        }

        ExchangeData(data);    
    }

    private void ExchangeData(ExchangedData[,] data)
    {
        for (int cntLine =0; cntLine< _gm.SubBoardLength;cntLine++)
        {
            for (int cntColumn = 0; cntColumn < _gm.SubBoardLength; cntColumn++)
            {
                string coordinate = _sb.MyCells[cntLine, cntColumn];
                if (data[cntLine,cntColumn].Content !="empty")
                {
                    if (_gm.FreePositions.Contains(coordinate))
                    _gm.FreePositions.Remove(coordinate);
                }
                else
                {
                    if (!_gm.FreePositions.Contains(coordinate))
                        _gm.FreePositions.Add(coordinate);
                }

                data[cntLine, cntColumn].SetData(_gm.FullBoard[coordinate]);
            }
        }

        
    }

  

}

public class ExchangedData
{
    public GameObject BallImage;
    public string Content;

    public ExchangedData()
    {
        BallImage = null;
        Content = "";
    }
    public void SetData(GameObject Cell)
    {
        if (BallImage!=null)
        BallImage.transform.SetParent(Cell.transform, false);
        Cell.GetComponent<CellInformation>().Content = Content;
    }
}
