﻿using UnityEngine;

[System.Serializable]
public class ColorElement {

    [SerializeField] private string _name;
    [SerializeField] private Color _color;
    [SerializeField] private int _order;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public Color Color
    {
        get { return _color; }
        set { _color = value; }
    }

    public int Order
    {
        get { return _order; }
        set { _order = value; }
    }

    public ColorElement()
    {
        _name = "";
        _color = new Color(0, 0, 0, 1);
        _order = -1;
    }

    public ColorElement(int order)
    {
        _name = "";
        _color = new Color(0, 0, 0, 1);
        _order = order;
    }

    public ColorElement(string name,Color color)
    {
        _name = name;
        _color = color;
        _order = -1;
    }

    public ColorElement Clone()
    {
        ColorElement c = new ColorElement();
        c.Name = _name;
        c.Color = _color;
        c.Order = _order;
        return c;
    }

}
