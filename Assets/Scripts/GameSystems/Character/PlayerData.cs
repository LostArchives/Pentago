﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour {

    private Animator anim;
    private GameObject _bioPanel;
    private GameObject _contentPanel;

    public Player Player;
    public Text PlayerId;
    public Text CharacterName;
    public Toggle IsBot;
    
    void Awake()
    {
        anim = transform.Find("AvatarPanel").GetComponent<Animator>();
        _bioPanel = transform.parent.GetComponent<PlayerList>().BioPanel;
        _contentPanel = transform.parent.GetComponent<PlayerList>().ContentPanel;
    }

	// Use this for initialization
	void Start () {
        IsBot.isOn = Player.IsBot;
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    public void PreviousAvatar()
    {
        anim.SetTrigger("PreviousAvatar");
    }

    public void NextAvatar()
    {
        anim.SetTrigger("NextAvatar");
    }

    public void SetCharacter(Character chara)
    {
        Player.Character = chara;
        CharacterName.text = chara.Name;
    }

    public void ShowMyBio()
    {
        _bioPanel.GetComponent<CharacterBio>().ShowBio(Player.Character);
        _contentPanel.GetComponent<Animator>().SetBool("Fade", true);
    }

    public void ToogleBot()
    {
        Player.IsBot = IsBot.isOn;
    }


}
