﻿using System;
using UnityEngine;

[System.Serializable]
public class Character : IClonable<Character> {

    [SerializeField] private string _name;
    [SerializeField] private Sprite _avatar;
    [SerializeField] private Sprite _portrait;
    [SerializeField] private string _bio;

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public Sprite Avatar
    {
        get { return _avatar; }
        set { _avatar = value; }
    }

    public Sprite Portrait
    {
        get { return _portrait; }
        set { _portrait = value; }
    }

    public string Bio
    {
        get { return _bio; }
        set { _bio = value; }
    }

   

    public Character()
    {
        _name = "";
        _bio = "";

    }

    public Character(string name, bool isbot)
    {
        _name = name;
        _bio = "";

    }

    public Character(string name,ColorElement pcolor, bool isbot)
    {
        _name = name;
        _bio = "";
       
    }

    public Character Clone()
    {
        Character clone = new Character
        {
            Name = _name,
            Avatar = _avatar,
            Portrait = _portrait,
            Bio = _bio
        };
        return clone;
    }
}
