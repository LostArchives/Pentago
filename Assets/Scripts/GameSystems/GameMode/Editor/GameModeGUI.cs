﻿using UnityEngine;
using UnityEditor;
using System;

public partial class GameModeEditor  {

    const float SPACE_SIZE = 10f;
    const float COLOR_BOX_HEIGHT = 25f;

    private void ViewGameMode(int index)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Mode Name : ", _textStyle, GUILayout.ExpandWidth(false));
        myDb[index].Name = GUILayout.TextField(myDb[index].Name,_textfieldStyle);
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(SPACE_SIZE);
        if (GUILayout.Button("Add Color", _buttonStyle))
        {
            myDb[index].AddColor(new ColorElement());
        }

        GUILayout.Space(SPACE_SIZE);

        for (int cntColor = 0; cntColor < myDb[index].CountColor(); cntColor++)
        {
            EditorGUILayout.BeginHorizontal();
            ViewColors(myDb[index], cntColor);
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(SPACE_SIZE);
        }

        GUILayout.Space(SPACE_SIZE);
        GUILayout.Space(SPACE_SIZE);
    }

    private void ViewColors(GameMode gm, int colorIndex)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label((gm[colorIndex].Order + 1) + ". ", _textStyle, GUILayout.ExpandWidth(false));
        gm[colorIndex].Name = EditorGUILayout.TextField(gm[colorIndex].Name, _textfieldStyle, GUILayout.ExpandWidth(true), GUILayout.Height(COLOR_BOX_HEIGHT));
        gm[colorIndex].Color = EditorGUILayout.ColorField(gm[colorIndex].Color, GUILayout.ExpandWidth(true),GUILayout.Height(COLOR_BOX_HEIGHT));
        

        if (GUILayout.Button("\u2191", _buttonStyle, GUILayout.Width(50f),GUILayout.Height(25f)))
        {
            int previousIndex = (colorIndex < 1) ? gm.CountColor() - 1 : colorIndex - 1;
            gm.Move(colorIndex, previousIndex);
        }

        if (GUILayout.Button("\u2193", _buttonStyle, GUILayout.Width(50f),GUILayout.Height(25f)))
        {
            int nextIndex = (colorIndex >= gm.CountColor() - 1) ? 0 : colorIndex + 1;
            gm.Move(colorIndex, nextIndex);
        }

        if (GUILayout.Button("X", _buttonStyle, GUILayout.Width(50f),GUILayout.Height(25f)))
        {
            gm.RemoveColor(gm[colorIndex]);
        }
        EditorGUILayout.EndHorizontal();
    }

    private void ViewGameRule(int index)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Required Alignment : ", _textStyle, GUILayout.ExpandWidth(false));
        myDb[index].RequiredAlignement = Convert.ToInt32(GUILayout.TextField(myDb[index].RequiredAlignement + "", _textfieldStyle,GUILayout.Height(25f), GUILayout.Width(25f)));
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(SPACE_SIZE);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Number of Players : ", _textStyle, GUILayout.ExpandWidth(false));
        GUILayout.Space(SPACE_SIZE);
        GUILayout.Label("Min : ", _textStyle, GUILayout.ExpandWidth(false));
        myDb[index].MinPlayer = Convert.ToInt32(GUILayout.TextField(myDb[index].MinPlayer + "", _textfieldStyle,GUILayout.Height(25f), GUILayout.Width(25f)));
        GUILayout.Space(SPACE_SIZE);
        GUILayout.Label("Max : "+ myDb[index].MaxPlayer, _textStyle, GUILayout.ExpandWidth(false));
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();

    }

    private void ViewOtherInfo(int index)
    {
        EditorGUILayout.BeginVertical();
        
        GUILayout.Label("Description : ", _textStyle, GUILayout.ExpandWidth(true));
        myDb[index].Description = GUILayout.TextArea(myDb[index].Description, _textareaStyle, GUILayout.Width(200f), GUILayout.Height(75f));
        GUILayout.FlexibleSpace();

        

        GUILayout.Label("Comment : ", _textStyle, GUILayout.ExpandWidth(true));
        myDb[index].Comment = GUILayout.TextArea(myDb[index].Comment, _textareaStyle, GUILayout.Width(200f), GUILayout.Height(75f));
        GUILayout.FlexibleSpace();


        EditorGUILayout.EndVertical();
    }

    private void ViewBoardLayout(int index)
    {
        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("SubBoard Per Line : ", _textStyle, GUILayout.ExpandWidth(false));
        myDb[index].SubBoardPerLine = Convert.ToInt32(GUILayout.TextField(myDb[index].SubBoardPerLine + "",_textfieldStyle));
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(SPACE_SIZE);


        GUILayout.Label("Total SubBoard : " + myDb[index].TotalSubBoard, _textStyle, GUILayout.ExpandWidth(true));
        GUILayout.Space(SPACE_SIZE);

        GUILayout.Label("SubBoard Length : " + myDb[index].SubBoardLength, _textStyle, GUILayout.ExpandWidth(true));
        GUILayout.Space(SPACE_SIZE);

        GUILayout.Label("Total Board Length : " + myDb[index].TotalBoardLength, _textStyle, GUILayout.ExpandWidth(true));
        GUILayout.Space(SPACE_SIZE);

        EditorGUILayout.EndVertical();
  
        GUILayout.Space(SPACE_SIZE);
    }

    private void ViewBoardModelizer(int index)
    {
        
        string modelizer = "  \n";
        for (int cntLine = 0; cntLine < myDb[index].SubBoardPerLine; cntLine++)
        {
            //string line = ((char)(cntLine + 97)+"");
            modelizer += SquareLine(cntLine + "", myDb[index].SubBoardPerLine);
            modelizer += "\n";

        }
        
        GUILayout.Label(modelizer);

    }

    private string SquareLine(string actualLine, int size)
    {
        string line = "";
        for (int cnt = 0; cnt < size; cnt++)
        {
              line += "|\u203E"+ actualLine + "\u203E|";
            //line += "|\u203E" + "\u203E" + "\u203E|";

        }
        line += "\n";

        for (int cnt = 0; cnt < size; cnt++)
        {
            line += "|_"+cnt+"_|";
            //line += "|_" + "_" + "_|";

        }
        return line;

    }

    

}
