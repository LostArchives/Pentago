﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor {

    GameManager g;
    bool collapsed;

    void Awake()
    {
        g = (GameManager)target;
    }
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        int index = 0;
        collapsed = EditorGUILayout.Foldout(collapsed, "Free Positions");
        if (collapsed)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("    Size ");
            
            GUI.enabled = false;
            GUILayout.TextField(g.FreePositions.Count+"",GUILayout.Width(425f));
            
            GUI.enabled = true;
            GUILayout.EndHorizontal();
            foreach (string s in g.FreePositions)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("    Element "+index);
                
                GUI.enabled = false;
                GUILayout.TextField(s,GUILayout.Width(425f));
                
                GUI.enabled = true;
                GUILayout.EndHorizontal();
                index++;
            }
        }
        
    }

}
