﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public class PalettoStateMachine : MonoBehaviour {

    private GameManager _gm;
    private int _actPlayerIndex;
    private bool finished = false;
    [SerializeField] GameObject BallPrefab;
    [SerializeField] Text StateText;
    [SerializeField]
    TurnList turnList;
    public PalettoState gameState;

    void Awake()
    {
        _gm = GameManager.Instance;
    }

    // Use this for initialization
    void Start () {
        _actPlayerIndex = 0;
        gameState = PalettoState.INIT;
	}
	
	// Update is called once per frame
	void Update () {
	    switch(gameState)
        {
            case PalettoState.IDLE:
                break;

            case PalettoState.INIT:
                StateText.text = _gm.Players[_actPlayerIndex].Character.Name + " joue";
                gameState = PalettoState.PROCEED;
                break;

            case PalettoState.PROCEED:
                if (_gm.Players[_actPlayerIndex].IsBot)
                    gameState = PalettoState.BOT;
                else
                    gameState = PalettoState.CHOOSE;
                break;

            case PalettoState.CHOOSE:
                gameState = PalettoState.BOT;
                break;

            case PalettoState.BOT:
                StartCoroutine(PlayBot());
                gameState = PalettoState.WAIT;
                break;

            case PalettoState.WAIT:
                if (finished)
                {
                    finished = false;
                    gameState = PalettoState.NEXT;
                }
                   
                break;

            case PalettoState.NEXT:
                NextTurn();
                StateText.text = _gm.Players[_actPlayerIndex].Character.Name + " joue";
                StartCoroutine(ScrollTurnList());
                gameState = PalettoState.IDLE;
                break;

            case PalettoState.END:
                StateText.text = "end of the game";
                enabled = false;
                break;
        }

	}

    #region PublicMethods

    public void PlaceBall(string position)
    {

        GameObject ball = Instantiate(BallPrefab.gameObject);
        ball.transform.SetParent(_gm.FullBoard[position].transform, false);

        ball.GetComponent<Image>().color = _gm.Players[_actPlayerIndex].Color;
        _gm.FullBoard[position].GetComponent<CellInformation>().Content = _gm.ActualColor.Name;
        _gm.FreePositions.Remove(position);

    }

    public void RandomSubboardSelection()
    {
        StartCoroutine(RandomSubBoard());
    }

    public void RandomBallSelection()
    {
        StartCoroutine(RandomBall());
    }

    #endregion

    #region PrivateMethods

    private void NextTurn()
    {
        _actPlayerIndex++;
        if (_actPlayerIndex > _gm.Players.Count - 1)
            _actPlayerIndex = 0;

        
    }

    private IEnumerator ScrollTurnList()
    {
        yield return StartCoroutine(turnList.ScrollToNext());
        gameState = PalettoState.PROCEED;
        
    }

    private IEnumerator PlayBot()
    {
        yield return StartCoroutine(RandomBall());
        yield return StartCoroutine(RandomSubBoard());
        finished = true;
    }

    private IEnumerator RandomSubBoard()
    {
        System.Random r = new System.Random();
        int randomIndex = 0;
        int newRandom = 0;
        for (int cntSubBoard = 0; cntSubBoard < 100; cntSubBoard++)
        {
            while (newRandom == randomIndex)
            {
                randomIndex = r.Next(0, _gm.SubBoards.Count);
                yield return null;
            }

            Animator anim = _gm.SubBoards[randomIndex].transform.parent.GetComponent<Animator>();
            anim.SetBool("Selected", true);
            if (cntSubBoard > 75)
                yield return new WaitForSeconds(cntSubBoard / 500f);
            else
                yield return new WaitForSeconds(cntSubBoard / 1000f);

            if (cntSubBoard != 99)
                anim.SetBool("Selected", false);

            yield return new WaitForSeconds(.02f);

            newRandom = randomIndex;

        }

        _gm.SelectedSubBoard = _gm.SubBoards[randomIndex];

        yield return null;
    }

    private IEnumerator RandomBall()
    {
        System.Random r = new System.Random();
        string randomCoordinate = RandomCoordinate(r);
        string newCoordinate = "";

        for (int cntCell = 0; cntCell < 100; cntCell++)
        {
            newCoordinate = randomCoordinate;
            while (randomCoordinate == newCoordinate)
            {
                randomCoordinate = RandomCoordinate(r);
                yield return null;
            }
            CellSelector cs = _gm.FullBoard[randomCoordinate].GetComponent<CellSelector>();
            cs.PreviewBall(_gm.Players[_actPlayerIndex].Color);

            if (cntCell > 75)
                yield return new WaitForSeconds(cntCell / 500f);
            else
                yield return new WaitForSeconds(cntCell / 1000f);

            cs.PreviewOff();

            yield return new WaitForSeconds(.02f);

            newCoordinate = randomCoordinate;

        }

        PlaceBall(randomCoordinate);

        yield return null;
    }

    private string RandomCoordinate(System.Random generator)
    {
        string coordinate = "";
        int randomIndex = generator.Next(0, _gm.FreePositions.Count);
        coordinate = _gm.FreePositions.ElementAt(randomIndex);
        return coordinate;
    }

    #endregion

}
