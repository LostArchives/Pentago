﻿public enum PalettoState
{
    IDLE,
    INIT,
    CHOOSE,
    PROCEED,
    BOT,
    NEXT,
    WAIT,
    END
}