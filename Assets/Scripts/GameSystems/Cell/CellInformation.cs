﻿using UnityEngine;

public class CellInformation : MonoBehaviour {

    private string _coordinate;
    private string _content;

    public string Coordinate
    {
        get { return _coordinate; }
        set { _coordinate = value; }
    }

    public string Content
    {
        get { return _content; }
        set { _content = value; }

    }

}
