﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CellSelector : MonoBehaviour {

    public GameObject BallPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PreviewBall(Color c)
    {
        if (gameObject.transform.childCount==0)
        {
            GameObject ball = Instantiate(BallPrefab.gameObject);
            ball.transform.SetParent(gameObject.transform, false);
            ball.GetComponent<Image>().color = c;
            GetComponent<CellInformation>().Content = ball.GetComponent<Image>().color.ToString();
        }
        
    }

    public void PreviewOff()
    {
        if (gameObject.transform.GetChild(0)!=null)
        Destroy(gameObject.transform.GetChild(0).gameObject);
        GetComponent<CellInformation>().Content = "empty";
    }
}
